import { createRoot } from 'react-dom/client';

createRoot(document.getElementById('root') as HTMLElement)
  .render(
    <div>Hello World</div>,
  );
