import { join } from 'path';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { WebpackConfiguration } from 'webpack-dev-server';
import TerserPlugin from 'terser-webpack-plugin';
import { ArgvMode, Mode } from '../@interfaces';

const getConfig = (env: Record<string, unknown>, argv: Record<string, unknown> & ArgvMode) => {
  const isDev = argv.mode === Mode.development;

  const config: WebpackConfiguration = {
    mode: isDev ? Mode.development : Mode.production,
    entry: {
      main: join(__dirname, 'index.tsx'),
    },
    output: {
      path: env?.WEBPACK_SERVE ? '/' : join(__dirname, 'public', 'dist'),
      clean: true,
    },
    optimization: {
      minimizer: [
        new TerserPlugin({ extractComments: false }),
      ],
    },
    module: {
      rules: [
        {
          test: /\.(s[ac]ss|css)$/i,
          use: [
            env?.WEBPACK_SERVE ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.tsx?$/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true,
              },
            },
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    devtool: false,
    plugins: [
      !env?.WEBPACK_SERVE && new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
    ],
    devServer: {
      hot: true,
      compress: true,
      historyApiFallback: {
        rewrites: [
          {
            from: /dist\/(.*)/,
            to: (context) => {
              return context.parsedUrl.href.replace('dist/', '');
            },
          },
        ],
      },
    },
  };
  return config;
};

export default getConfig;
